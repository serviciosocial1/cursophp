<!-- Herencia en PHP -->

<?php

class persona {

    // Propiedades
    public $nombre;
    private $edad;          
    protected $altura;
    // Metodos
    public function asignarNombre($nuevoNomnre){
        $this->nombre = $nuevoNomnre;
    }
    public function imprimirNombre(){
        echo "Hola soy: $this->nombre <br>";
    } 
    public function mostrarEdad($edad){
        $this->edad = $edad;
        return $this->edad;
    }
    public function mostrarAltura($altura){
        $this->$altura = $altura;
        return $this->$altura;
    }
}

class trabajador extends persona {

    // Propiedades
    public $puesto;

    public function presentacionTrab(){
        echo "<br> Hola soy $this->nombre y soy un $this->puesto";
    }
}

$objTrab01 = new trabajador();
$objTrab01->asignarNombre('Oscar G');
$objTrab01->puesto = 'Profesor';
$objTrab01->presentacionTrab();
echo '<br> altura: '.$objTrab01->mostrarAltura(189);

// echo $objTrab01->altura; //Error

?>