<!-- Clases en PHP -->

<?php

class persona {

    // Propiedades
    public $nombre;
    private $edad;          
    protected $altura;
    // Metodos
    public function asignarNombre($nuevoNomnre){
        $this->nombre = $nuevoNomnre;
    }
    public function imprimirNombre(){
        echo "Hola soy: $this->nombre <br>";
    } 
    public function mostrarEdad($edad){
        $this->edad = $edad;
        return $this->edad;
    }
    public function mostrarAltura($altura){
        $this->$altura = $altura;
        return $this->$altura;
    }
}

// Instancia
$objAlumno1 = new persona();
$objAlumno2 = new persona();

// metodo
$objAlumno1->asignarNombre('Oscar');
$objAlumno2->asignarNombre('Kimba');

// Visibilidad public
echo "$objAlumno1->nombre <br>";
echo "$objAlumno2->nombre <br>";

// metodo public
$objAlumno1->imprimirNombre();
$objAlumno2->imprimirNombre();

// Visibilidad Private
// echo $objAlumno1->edad; //Error
// echo $objAlumno1->edad; //Error

// Visibilidad Protect
// echo $objAlumno1->altura; //Error
// echo $objAlumno2->altura; //Error

echo $objAlumno1->mostrarEdad(20).'<br>';
echo $objAlumno2->mostrarEdad(30).'<br>';
echo $objAlumno2->mostrarAltura(189).'<br>';
echo $objAlumno2->mostrarAltura(175).'<br>';

?>