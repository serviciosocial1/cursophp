<!-- OPERADORES ARITMETICOS -->

<?php

if ($_POST) {
    $valueA = $_POST['textValueA'];
    $valueB = $_POST['textValueB'];

    //  Operaciones
    echo "A = $valueA <br>";
    echo "B = $valueB <br>";
    echo "A + B = ".$valueA + $valueB ."<br>";
    echo "A - B = ".$valueA - $valueB ."<br>";
    echo "A * B = ".$valueA * $valueB ."<br>";
    echo "A / B = ".$valueA / $valueB ."<br>";
    echo "A % B = ".$valueA % $valueB ."<br>";
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>
<body>
    <form action="ejercicio07.php" method="POST">
        Value A:
        <input type="text" name="textValueA"><br>
        Value B:
        <input type="text" name="textValueB"><br>
        <input type="submit" value="Calc">
    </form>
</body>
</html>