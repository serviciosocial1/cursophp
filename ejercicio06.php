<!-- VARIABLES Y CONSTANTES -->

<?php
// Variable
$edad = 33;
echo "$edad <br>";
$edad = 48;
echo "$edad <br>";

// Constantes

define("NOMBRE", "Oscar");
echo NOMBRE;

// No se puede modificar
// NOMBRE = "Meche"; 

echo NOMBRE;

?>